import React from "react";

class ManufacturerList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            manufacturers_records: []
        };
    }

     //endpoints
     static salesRecordUrl = 'http://localhost:8100/api/manufacturers/';

     async componentDidMount() {
         const response = await fetch("http://localhost:8100/api/manufacturers/");
         if (response.ok) {
             const data = await response.json()
             this.setState({ manufacturers_records: data.manufacturers });
         }
     }

render() {
    // List of manufacturers
    return (
        <> 
    <table className="table table-striped">
        <thead>
        <tr>
            <th>Manufacturers</th>
        </tr>
        </thead>
        <tbody>
        {this.state.manufacturers_records ? this.state.manufacturers_records.map(man => {
                            return (
                                <tr key={man.id}>
                                    <td>{man.name}</td>
                                </tr>
                            );
                        }):null}
        </tbody>
    </table>
        </> 
    );
    }
}

export default ManufacturerList;
import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
      <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <div className='p-2 dropdown'>
          <NavLink
              className='btn btn-outline-success dropdown-toggle'
            to='#'
            role='button'
            id='dropdownMenu'
            data-bs-toggle='dropdown'
            aria-haspopup='true'
            aria-expanded='false'
          >
            Sales
          </NavLink>
          <div className='dropdown-menu' aria-labelledby='dropdownMenu'>
          <NavLink className="nav-link" to="/salesreps/history/">Sales History</NavLink>
              <li className='dropdown-divider' />
           <NavLink className="nav-link" to="/customers/new/">Add Customer</NavLink>
              <li className='dropdown-divider' />
              <NavLink className="nav-link" to="/salesreps/new/">Add Sales Person</NavLink>
              <li className='dropdown-divider' />
              <NavLink className="nav-link" to="/salesrecord/new/">Make a Sale</NavLink>
              <li className='dropdown-divider' />
              <NavLink className="nav-link" to='/salesreps/history/'>
              Filter sales by employee
            </NavLink>
          </div>
        </div>
        <div className='p-2 dropdown'>
          <NavLink
              className='btn btn-outline-success dropdown-toggle'
            to='#'
            role='button'
            id='dropdownMenu'
            data-bs-toggle='dropdown'
            aria-haspopup='true'
            aria-expanded='false'
          >
            Inventory
          </NavLink>
          <div className='dropdown-menu' aria-labelledby='dropdownMenu'>
          <NavLink className="nav-link" to="/automobile/list/">Automobiles List</NavLink>
              <li className='dropdown-divider' />
              <NavLink className="nav-link" to="/model/list/">Vehicle Models</NavLink>
              <li className='dropdown-divider' />
              <NavLink className="nav-link" to="/manufacturer/list/">Manufacturer List</NavLink>
              <li className='dropdown-divider' />
              <NavLink className="nav-link" to="/automobile/new/">New Automobile</NavLink>
              <li className='dropdown-divider' />
              <NavLink className="nav-link" to="/model/new/">New Model</NavLink>
              <li className='dropdown-divider' />
              <NavLink className="nav-link" to="/manufacturer/new/">New Manufacturer</NavLink>
          </div>
          </div>
          <div className="dropdown p-2">
            <NavLink
              className='btn btn-outline-success dropdown-toggle'
              to='#'
              role='button'
              id='dropdownMenu'
              data-bs-toggle='dropdown'
              aria-haspopup='true'
              aria-expanded='false'>Services</NavLink>
            <ul className="dropdown-menu me-auto mb-2 mb-lg-0" aria-labelledby="dropdownMenuButton2">
              <NavLink className="dropdown-item" aria-current="page" to="/ApptList/">Appointments</NavLink>
              <li className='dropdown-divider' />
              <li className="nav-item">
                <NavLink className="nav-link" to="/ApptList/new/">New Appointment</NavLink>
            </li>
              <li className='dropdown-divider' />
              <li className="nav-item">
                <NavLink className="nav-link" to="/techs/new/">Add New Tech</NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link" to="/ApptList/ServiceHistory/">Appoints by VIN</NavLink>
            </li>
            </ul>
          </div>
        </div>
      </div>
    </nav>
  )
}

export default Nav;

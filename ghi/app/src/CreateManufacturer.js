import React from 'react'

class ManufacturerForm extends React.Component {
constructor(props) {
    super(props)
    this.state = {
    name: ''
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
}

//endpoints
static conferenceUrl = 'http://localhost:8100/api/manufacturers/'

async handleSubmit(event) {
    event.preventDefault()
    const data = { ...this.state }
    console.log(data)


    const fetchConfig = {
    method: "POST",
    body: JSON.stringify(data),
    headers: {
        'Content-Type': 'application/json',
    },
    }
    const response = await fetch("http://localhost:8100/api/manufacturers/", fetchConfig)
    if (response.ok) {
        const newMan = await response.json()
        console.log("manufacturer data: ", newMan)

        const empty_obj = {
            name: ''
        }
        this.setState(empty_obj)
        }
}

handleChange(event) {
    this.setState({
    [event.target.name]: event.target.value
    });
}

render() {
    return (
    <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Create a manufacturer</h1>
            <form onSubmit={this.handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
                <input value={this.state.name} onChange={this.handleChange} placeholder="name" required type="text" id="name" name="name" className="form-control" />
                <label htmlFor="name">Manufacturer Name</label>
            </div>
            <button className="btn btn-primary">Create</button>
            </form>
        </div>
        </div>
    </div>
    )
}
}

export default ManufacturerForm
